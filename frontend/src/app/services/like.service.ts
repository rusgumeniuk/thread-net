import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment'
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CommentService } from './comment.service';
import { UserService } from './user.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({ providedIn: 'root' })
export class LikeService {

    public constructor(
        private authService: AuthenticationService,
        private postService: PostService,
        private commentService: CommentService,
        private userService: UserService,
        private toastrService: ToastrService
    ) { }

    public likePost(post: Post, currentUser: User) {
        return this.reactToPost(post, currentUser, true);
    }

    public dislikePost(post: Post, currentUser: User) {
        return this.reactToPost(post, currentUser, false);
    }

    public likeComment(comment: Comment, currentUser: User) {
        return this.reactToComment(comment, currentUser, true);
    }

    public dislikeComment(comment: Comment, currentUser: User) {
        return this.reactToComment(comment, currentUser, false);
    }

    public reactToPost(post: Post, currentUser: User, isLike: boolean) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: isLike,
            userId: currentUser.id
        };

        // update current array instantly
        let existReaction = innerPost.reactions.find(reaction => reaction.user.id === currentUser.id);
        let isExistSameReaction = existReaction && existReaction.isLike === isLike;

        if (isExistSameReaction) {
            innerPost.reactions = innerPost.reactions.filter(reaction => reaction.user.id !== currentUser.id);
        }
        else if (existReaction) {
            innerPost.reactions = innerPost.reactions.filter(reaction => reaction.user.id !== currentUser.id);
            innerPost.reactions = innerPost.reactions.concat({ isLike: isLike, user: currentUser });
        }
        else {
            innerPost.reactions = innerPost.reactions.concat({ isLike: isLike, user: currentUser });
        }

        if (!isExistSameReaction && isLike) {
            this.userService.notify(post.author);
        }

        isExistSameReaction = innerPost.reactions.some(reaction => reaction.user.id === currentUser.id && reaction.isLike === isLike);

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                if (isExistSameReaction) {
                    innerPost.reactions = innerPost.reactions.filter(reaction => reaction.user.id !== currentUser.id);
                }
                else if (existReaction) {
                    innerPost.reactions = innerPost.reactions.filter(reaction => reaction.user.id !== currentUser.id);
                    innerPost.reactions = innerPost.reactions.concat({ isLike: isLike, user: currentUser });
                }
                else {
                    innerPost.reactions = innerPost.reactions.concat({ isLike: isLike, user: currentUser });
                }

                return of(innerPost);
            })
        );
    }

    public reactToComment(comment: Comment, currentUser: User, isLike: boolean) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: isLike,
            userId: currentUser.id
        };

        // update current array instantly
        let existReaction = innerComment.reactions.find(reaction => reaction.user.id === currentUser.id);
        let isExistSameReaction = existReaction && existReaction.isLike === isLike;

        if (isExistSameReaction) {
            innerComment.reactions = innerComment.reactions.filter(reaction => reaction.user.id !== currentUser.id);
        }
        else if (existReaction) {
            innerComment.reactions = innerComment.reactions.filter(reaction => reaction.user.id !== currentUser.id);
            innerComment.reactions = innerComment.reactions.concat({ isLike: isLike, user: currentUser });
        }
        else {
            innerComment.reactions = innerComment.reactions.concat({ isLike: isLike, user: currentUser });
        }

        isExistSameReaction = innerComment.reactions.some(reaction => reaction.user.id === currentUser.id && reaction.isLike === isLike);

        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                if (isExistSameReaction) {
                    innerComment.reactions = innerComment.reactions.filter(reaction => reaction.user.id !== currentUser.id);
                }
                else if (existReaction) {
                    innerComment.reactions = innerComment.reactions.filter(reaction => reaction.user.id !== currentUser.id);
                    innerComment.reactions = innerComment.reactions.concat({ isLike: isLike, user: currentUser });
                }
                else {
                    innerComment.reactions = innerComment.reactions.concat({ isLike: isLike, user: currentUser });
                }

                return of(innerComment);
            })
        );
    }
}
