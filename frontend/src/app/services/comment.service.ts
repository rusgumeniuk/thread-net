import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Comment } from '../models/comment/comment';
import { NewComment } from '../models/comment/new-comment';
import { NewReaction } from '../models/reactions/newReaction';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) { }

    public createComment(comment: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, comment);
    }

    public likeComment(reaction: NewReaction) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/like`, reaction);
    }

    public updateComment(comment: Comment) {
        return this.httpService.putFullRequest<Comment>(`${this.routePrefix}/update`, comment)
    }

    public deleteComment(id: number) {
        return this.httpService.deleteRouteRequest(`${this.routePrefix}/delete`, id);
    }
}
