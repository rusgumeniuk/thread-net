import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { PostService } from 'src/app/services/post.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { ImgurService } from 'src/app/services/imgur.service';
import { Reaction } from 'src/app/models/reactions/reaction';
import { ToastrService } from 'ngx-toastr';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { NewReaction } from 'src/app/models/reactions/newReaction';


@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy, OnInit {
    @Input()
    public post: Post;

    public postCopy: Post;

    public likes: Reaction[];
    public disLikes: Reaction[];

    @Input()
    public currentUser: User;

    public isOwned: boolean;

    public isEditMode: boolean;

    public imageUrl: string;
    public imageFile: File;
    public showPostContainer = false;
    public loading = false;

    public showComments = false;
    public newComment = {} as NewComment;

    public postHub: HubConnection;

    private unsubscribe$ = new Subject<void>();

    @Output()
    public onDelete = new EventEmitter<Post>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private postService: PostService,
        private imgurService: ImgurService,
        private snackBarService: SnackBarService,
        private toastrService: ToastrService
    ) { }

    ngOnInit(): void {
        this.registerHub();
        this.isOwned = this.currentUser && this.post.author.id === this.currentUser.id;
        this.isEditMode = false;
        this.updateReactions();
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/post').build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));


        this.postHub.on('NewComment', (newComment: Comment) => {
            if (newComment) {
                if (newComment.postId === this.post.id) {
                    //this.addNewComment(newComment);
                }
            }
        });

        this.postHub.on('UpdatePost', (updatedPost: Post) => {
            if (updatedPost.id === this.post.id) {
                this.post = updatedPost;
                if (updatedPost.author.id === this.currentUser.id) {
                    this.toastrService.success("Post updated!");
                }
            }
        });

        // this.postHub.on('DeletePost', (deletedPost: Post) => {
        //     if (deletedPost.id === this.post.id) {
        //         // if (this.post.author.id === this.currentUser.id) {
        //         //     this.toastrService.error("Post deleted!");
        //         // }
        //     }
        // });

        this.postHub.on('PostReaction', (reaction: NewReaction) => {
            if (reaction.userId === this.currentUser.id
                && reaction.entityId === this.post.id
                && this.post.reactions
                    .find(_reaction => _reaction.user.id === reaction.userId && _reaction.isLike === reaction.isLike)
            ) {
                if (reaction.isLike) {
                    this.toastrService.success("You liked a post!");
                }
                else {
                    this.toastrService.error("You disliked a post.");
                }
            }
        });
    }

    // private addNewComment(newComment: Comment) {
    //     if (!this.post.comments.some((x) => x.id === newComment.id)) {
    //         this.post.comments = this.sortCommentArray(this.post.comments.concat(newComment));
    //     }
    // }


    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }
        this.showComments = !this.showComments;
    }


    public likePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));
            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
        this.updateReactions();
    }


    public dislikePost() {

        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));
            return;
        }

        this.likeService
            .dislikePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
        this.updateReactions();
    }


    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;
        let commentBody = this.newComment.body;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => {
                    this.snackBarService.showErrorMessage(error);
                }
            );
        this.toastrService.success(commentBody, "Comment sent!")
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }


    public deletePost(): void {
        this.postService
            .deletePost(this.post.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.onDelete.emit(this.post);
                        this.ngOnDestroy();
                    }
                },
                (error) => {
                    this.snackBarService.showErrorMessage(error);
                }
            );
    }

    public editPost(): void {
        this.postCopy = Object.assign({}, this.post);
        this.isEditMode = true;
        this.showComments = false;
    }

    public updatePost(): void {
        const postSubscription = !this.imageFile
            ? this.postService.updatePost(this.post)
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.body.data.link;
                    return this.postService.updatePost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            () => {
                this.postCopy = Object.assign({}, this.post);
                this.finishEditing();
            },
            (error) => {
                this.snackBarService.showErrorMessage(error);
            }
        );
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
        this.post.previewImage = this.imageUrl;
    }

    public removeImage() {
        this.post.previewImage = undefined;
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public cancel() {
        this.post = Object.assign({}, this.postCopy);
        this.finishEditing();
    }

    private finishEditing() {
        this.isEditMode = false;
        this.imageFile = undefined;
        this.imageUrl = undefined;
        this.loading = false;
    }

    private updateReactions() {
        this.likes = this.post.reactions.filter(_reaction => _reaction.isLike);
        this.disLikes = this.post.reactions.filter(_reaction => !_reaction.isLike);
    }

    public onDeleteComment(comment: Comment) {
        this.post.comments = this.sortCommentArray(this.post.comments.filter(_comment => _comment.id !== comment.id));
    }
}
