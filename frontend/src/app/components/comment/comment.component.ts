import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from 'src/app/models/user';
import { CommentService } from '../../services/comment.service';
import { LikeService } from '../../services/like.service';
import { AuthenticationService } from "../../services/auth.service";
import { AuthDialogService } from "../../services/auth-dialog.service";
import { DialogType } from "../../models/common/auth-dialog-type";
import { empty, Observable, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Post } from 'src/app/models/post/post';
import { Reaction } from 'src/app/models/reactions/reaction';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { ToastrService } from 'ngx-toastr';
import { NewReaction } from 'src/app/models/reactions/newReaction';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnDestroy, OnInit {
    @Input()
    public comment: Comment;

    @Input()
    public currentUser: User;

    @Input()
    public post: Post;

    public likes: Reaction[];
    public disLikes: Reaction[];

    public isEditMode: boolean;
    public isOwned: boolean;
    public commentCopy: Comment;

    @Output()
    public onDelete = new EventEmitter<Comment>();

    public postHub: HubConnection;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private toastrService: ToastrService
    ) { }

    public ngOnInit() {
        this.registerHub();
        this.isOwned = this.currentUser && this.comment.author.id === this.currentUser.id;
        this.isEditMode = false;
        this.comment.postId = this.post.id;
        this.updateReactions();
    }


    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/post').build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on('UpdateComment', (updateComment: Comment) => {
            if (updateComment.id === this.post.id) {
                this.comment = updateComment;
                if (updateComment.author.id === this.currentUser.id) {
                    this.toastrService.success("Comment updated!");
                }
            }
        });

        this.postHub.on('DeleteComment', (deleteComment: Comment) => {
            if (deleteComment.id === this.comment.id) {
                //this.deleteComment();
                if (this.comment.author.id === this.currentUser.id) {
                    this.toastrService.error("Comment deleted!");
                }
            }
        });

        this.postHub.on('CommentReaction', (reaction: NewReaction) => {
            if (reaction.userId === this.currentUser.id
                && reaction.entityId === this.comment.id
                && this.comment.reactions
                    .find(_reaction => _reaction.user.id === reaction.userId && _reaction.isLike === reaction.isLike)
            ) {
                if (reaction.isLike) {
                    this.toastrService.success("You liked a comment!");
                }
                else {
                    this.toastrService.error("You disliked a comment.");
                }
            }
        });
    }


    public cancel() {
        this.comment = Object.assign({}, this.commentCopy);
        this.finishEditing();
    }

    public editComment() {
        this.commentCopy = Object.assign({}, this.comment);
        this.isEditMode = true;
    }

    public updateComment() {
        this.commentService
            .updateComment(this.comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.commentCopy = Object.assign({}, this.comment);
                        this.finishEditing();
                    }
                },
                (error) => {
                    this.snackBarService.showErrorMessage(error);
                }
            );
    }

    private finishEditing() {
        this.isEditMode = false;
    }

    public deleteComment() {
        this.commentService
            .deleteComment(this.comment.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.onDelete.emit(this.comment);
                        this.ngOnDestroy();
                    }
                },
                (error) => {
                    this.snackBarService.showErrorMessage(error);
                }
            );
    }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
        this.updateReactions();
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));
            return;
        }

        this.likeService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
        this.updateReactions();
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private updateReactions() {
        this.likes = this.comment.reactions.filter(_reaction => _reaction.isLike);
        this.disLikes = this.comment.reactions.filter(_reaction => !_reaction.isLike);
    }
}
