﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly CommentService _commentService;
        private readonly LikeService _likeService;

        public CommentsController(CommentService commentService, LikeService likeService)
        {
            _commentService = commentService;
            _likeService = likeService;
        }

        [HttpPost]
        public async Task<ActionResult<CommentDTO>> CreateCommentAsync([FromBody] NewCommentDTO comment)
        {
            comment.AuthorId = this.GetUserIdFromToken();
            return Ok(await _commentService.CreateCommentAsync(comment));
        }

        [HttpPut("update")]
        public async Task<ActionResult<CommentDTO>> UpdateCommentAsync([FromBody] CommentDTO comment)
        {
            return Ok(await _commentService.UpdateCommentAsync(comment));
        }

        [HttpDelete("delete/{id}")]
        public async Task DeleteCommentAsync([FromRoute] int id)
        {
            await _commentService.DeleteCommentAsync(id);
        }

        [HttpPost("like")]
        public async Task<IActionResult> LikeCommentAsync(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _likeService.LikeCommentAsync(reaction);
            return Ok();
        }

        [HttpGet("respond")]
        public async Task<ActionResult<ReactionDTO>> GetRespondedPeopleAsync([FromBody] int commentId)
        {
            return Ok(await _likeService.GetRespondedToCommentPeopleAsync(commentId));
        }
    }
}