﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Threading.Tasks;

using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly LikeService _likeService;

        public PostsController(PostService postService, LikeService likeService)
        {
            _postService = postService;
            _likeService = likeService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Get()
        {
            return Ok(await _postService.GetAllPostsAsync());
        }

        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePostAsync([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePostAsync(dto));
        }

        [HttpPost("like")] //TODO create DislikePost method or just use IsLike = false?
        public async Task<IActionResult> LikePostAsync(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _likeService.LikePostAsync(reaction);
            return Ok();
        }

        [HttpPut("update")]
        public async Task<ActionResult<PostDTO>> UpdatePostAsync([FromBody] PostDTO dto)
        {
            return Ok(await _postService.UpdatePostAsync(dto));
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeletePostAsync([FromRoute] int id)
        {
            await _postService.DeletePostAsync(id);
            return NoContent();
        }

        [HttpGet("respond")]
        public async Task<ActionResult<ReactionDTO>> GetRespondedPeopleAsync([FromBody] int postId)
        {
            return Ok(await _likeService.GetRespondedToPostPeopleAsync(postId));
        }
    }
}