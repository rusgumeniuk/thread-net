﻿using AutoMapper;

using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public LikeService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task LikePostAsync(NewReactionDTO reactionDto)
        {
            var likes = await _context
                .PostReactions
                .Where(postReaction => postReaction.UserId == reactionDto.UserId
                                       && postReaction.PostId == reactionDto.EntityId
                                       && !postReaction.Post.IsDeleted)
                .ToListAsync(); //TODO why likes not like?

            if (likes.Any()) //TODO ???
            {
                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }

            _context.PostReactions.Add(new PostReaction
            {
                PostId = reactionDto.EntityId,
                IsLike = reactionDto.IsLike,
                UserId = reactionDto.UserId
            });

            await _postHub.Clients.All.SendAsync("PostReaction", reactionDto);

            await _context.SaveChangesAsync();
        }

        public async Task LikeCommentAsync(NewReactionDTO reactionDto)
        {
            var likes = await _context
                .CommentReactions
                .Where(commentReaction => commentReaction.UserId == reactionDto.UserId
                                          && commentReaction.CommentId == reactionDto.EntityId
                                          && !commentReaction.Comment.IsDeleted)
                .ToListAsync();//TODO why likes not like?

            if (likes.Any())
            {
                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();
                return;
            }

            _context.CommentReactions.Add(new CommentReaction()
            {
                CommentId = reactionDto.EntityId,
                IsLike = reactionDto.IsLike,
                UserId = reactionDto.UserId
            });

            await _postHub.Clients.All.SendAsync("CommentReaction", reactionDto);

            await _context.SaveChangesAsync();
        }


        public async Task<ICollection<ReactionDTO>> GetRespondedToPostPeopleAsync(int postId)
        {
            return await _context
                .PostReactions
                .Include(reaction => reaction.User)
                    .ThenInclude(author => author.Avatar)
                .Where(reaction => reaction.PostId == postId && !reaction.Post.IsDeleted)
                .Select(reaction => _mapper.Map<ReactionDTO>(reaction))
                .ToListAsync();
        }

        public async Task<ICollection<ReactionDTO>> GetRespondedToCommentPeopleAsync(int commentId)
        {
            return await _context
                .CommentReactions
                .Include(reaction => reaction.User)
                    .ThenInclude(author => author.Avatar)
                .Where(reaction => reaction.CommentId == commentId && !reaction.Comment.IsDeleted)
                .Select(reaction => _mapper.Map<ReactionDTO>(reaction))
                .ToListAsync();
        }

        public async Task<ICollection<PostDTO>> GetLikedPostAsync(int userId)
        {
            return await _context
                .PostReactions
                .Where(reaction => reaction.UserId == userId && !reaction.Post.IsDeleted)
                .Select(reaction => _mapper.Map<PostDTO>(reaction.Post))
                .ToListAsync();
        }
    }
}
