﻿using AutoMapper;

using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

using System.Threading.Tasks;

using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        public CommentService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<CommentDTO> CreateCommentAsync(NewCommentDTO newCommentDto)
        {
            var commentEntity = _mapper.Map<Comment>(newCommentDto);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            var createdCommentDto = _mapper.Map<CommentDTO>(createdComment);
            await _postHub.Clients.All.SendAsync("NewComment", createdCommentDto);

            return createdCommentDto;
        }

        public async Task<CommentDTO> UpdateCommentAsync(CommentDTO commentDto)
        {
            var commentEntity = _mapper.Map<Comment>(commentDto);

            var oldComment = await GetCommentAsync(commentEntity.Id);

            if (oldComment == null)
            {
                return null;
            }

            _context.Entry(oldComment).CurrentValues.SetValues(commentEntity);
            await _context.SaveChangesAsync();

            var updatedComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(author => author.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            var updatedCommentDto = _mapper.Map<CommentDTO>(updatedComment);
            await _postHub.Clients.All.SendAsync("UpdateComment", updatedCommentDto);

            return updatedCommentDto;
        }

        private async Task<Comment> GetCommentAsync(int commentId)
        {
            var res = await _context.Comments.FindAsync(commentId);
            return res?.IsDeleted ?? true ? null : res;
        }

        public async Task DeleteCommentAsync(int commentId)
        {
            var commentEntity = await GetCommentAsync(commentId);
            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), commentId);
            }

            commentEntity.IsDeleted = true;
            _context.Entry(commentEntity).CurrentValues.SetValues(commentEntity);

            var commentDto = _mapper.Map<CommentDTO>(commentEntity);
            await _postHub.Clients.All.SendAsync("DeleteComment", commentDto);

            await _context.SaveChangesAsync();
        }
    }
}
