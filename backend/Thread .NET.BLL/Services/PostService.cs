﻿using AutoMapper;

using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPostsAsync()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(post => !post.IsDeleted)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPostsAsync(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(post => post.AuthorId == userId && !post.IsDeleted) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePostAsync(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)

                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDto = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDto);

            return createdPostDto;
        }

        public async Task<PostDTO> UpdatePostAsync(PostDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);
            var oldPost = await GetPostAsync(postEntity.Id);
            if (oldPost == null)
            {
                return null;
            }

            _context.Entry(oldPost).CurrentValues.SetValues(postEntity);
            _context.Entry(oldPost).Reference(post => post.Preview).CurrentValue = postEntity.Preview;
            await _context.SaveChangesAsync();

            var updatedPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);
            
            var updatedPostDto = _mapper.Map<PostDTO>(updatedPost);
            await _postHub.Clients.All.SendAsync("UpdatePost", updatedPostDto);

            return updatedPostDto;
        }

        private async Task<Post> GetPostAsync(int postEntityId)
        {
            var res = await _context.Posts.FindAsync(postEntityId);
            return res?.IsDeleted ?? true ? null : res;
        }

        public async Task DeletePostAsync(int postId)
        {
            var postEntity = await GetPostAsync(postId);

            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post), postId);
            }

            postEntity.IsDeleted = true;
            _context.Entry(postEntity).CurrentValues.SetValues(postEntity);
            _context.Entry(postEntity).Reference(post => post.Preview).CurrentValue = postEntity.Preview;

            await _postHub.Clients.All.SendAsync("DeletePost", _mapper.Map<PostDTO>(postEntity));
            
            await _context.SaveChangesAsync();
        }
    }
}
