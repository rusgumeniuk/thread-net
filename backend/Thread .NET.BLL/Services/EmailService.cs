﻿
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace Thread_.NET.BLL.Services
{
    public class EmailService
    {
        private readonly IConfiguration _configuration;
        public EmailService(IConfiguration config)
        {
            _configuration = config;
        }
        public async Task SendEmailAsync(string emailTo, string text)
        {
            try
            {
                var message = new MailMessage
                {
                    IsBodyHtml = true,
                    From = new MailAddress("thread.net.project@gmail.com", "ThreadNet")
                };
                message.To.Add(emailTo);
                message.Subject = "Someone liked you post!";
                message.Body = $"<div style=\"color: blue;\">{text}</div>";

                using (SmtpClient client
                    = new SmtpClient("smtp.gmail.com"))
                {
                    client.Credentials = new NetworkCredential(
                        "thread.net.project@gmail.com",
                        "threadnet2020"
                        );
                    client.Port = 587; //587\465
                    client.EnableSsl = true;

                    await client.SendMailAsync(message);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }
    }
}
