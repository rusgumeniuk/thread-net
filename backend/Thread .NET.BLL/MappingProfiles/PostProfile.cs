﻿using AutoMapper;

using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.MappingProfiles
{
    public sealed class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<Post, PostDTO>()
                .ForMember(dest => dest.PreviewImage,
                    src => src
                        .MapFrom(post => post.Preview != null ? post.Preview.URL : string.Empty));

            CreateMap<PostDTO, Post>()
                .ForMember(dest => dest.Preview,
                    src => src
                        .MapFrom(dto => string.IsNullOrEmpty(dto.PreviewImage) ? null : new Image { URL = dto.PreviewImage }))
                //.ForMember(dest => dest.Comments,
                //    src => src
                //        .MapFrom(dto => dto.Comments.Select(comment => comment)))
                ;

            CreateMap<PostCreateDTO, Post>()
                .ForMember(dest => dest.Preview,
                    src => src
                        .MapFrom(dto => string.IsNullOrEmpty(dto.PreviewImage) ? null : new Image { URL = dto.PreviewImage }));
        }
    }
}
